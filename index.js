	const express = require('express');

	const app = express();

	const port = 3000;

	app.use(express.json());

	app.use(express.urlencoded({extended:true}));

	app.listen(port, () => console.log(`Server running at port ${port}`));

	let users = [
		{
			username: "User1",
			password: "password1"
		},{
			username: "User2",
			password: "password2"
		},{
			username: "User3",
			password: "password3"
		},
		];

	app.get("/home", (req,res) => {
		res.send("Welcome to Home Page");
	});

	app.get("/users", (req,res) => {
		res.send(users);
	});

	app.delete("/delete-user", (req,res) => {
		console.log(users);
		for(let i = 0; i < users.length; i++){
			if(users[i].username == req.body.username){
				users.splice(i, 1);
				break;
			}else{
				res.send(`${req.body.username} was not deleted`);
				break;
			}
		}
		res.send(`${req.body.username} was deleted`);
		console.log(users);
	});